About this project:

    Monero is open source and completely free to use without restrictions. There are no restrictions on anyone creating an alternative implementation of Monero that uses the protocol and network in a compatible manner.
As with many development projects, the repository on Gitlab is considered to be the "staging" area for the latest changes. 
Anyone is welcome to contribute to Monero's codebase!
Monero is a 100% community-sponsored endeavor. 
===> join us!